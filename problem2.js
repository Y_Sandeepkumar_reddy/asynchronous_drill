const fs = require('fs');
const path = require('path');


function problem_2() {
    // 1. Reading the given file lipsum.txt
    fs.readFile("lipsum.txt", "utf8", (err, data) => {
        if (err) {
            console.log(err);
        } else {
            console.log(data);
            console.log("Reading of data is successful");
            console.log("**************");

            // 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
            const uppercase = data.toUpperCase();
            console.log(uppercase);
            fs.writeFile("lipsum_uppercase.txt", uppercase, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Converted to uppercase successfully");
                    console.log("********************");

                    // Append the name of the new file to filenames.txt
                    fs.writeFile("filenames.txt", "lipsum_uppercase.txt\n", (err) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log("Filenames updated in filenames.txt");

                            // 3. Read the new file and convert it to lowercase. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
                            fs.readFile("lipsum_uppercase.txt", "utf8", (err, data) => {
                                if (err) {
                                    console.log(err);
                                } else {
                                    const lowercase = data.toLowerCase();

                                    // Split the content into sentences
                                    const sentences = lowercase.split(/[.!?]/);

                                    // Filter out empty sentences
                                    const filteredSentences = sentences.filter(sentence => sentence.trim() !== "");

                                    // Join the sentences back with newlines
                                    const textToWrite = filteredSentences.join("\n");

                                    // Write the processed content to a new file
                                    fs.writeFile("lipsum_lowercase_sentences.txt", textToWrite, (err) => {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            console.log("Converted to lowercase and split into sentences successfully");
                                            console.log("********************");

                                            // Append the name of the new file to filenames.txt
                                            fs.appendFile("filenames.txt", "lipsum_lowercase_sentences.txt\n", (err) => {
                                                if (err) {
                                                    console.log(err);
                                                } else {
                                                    console.log("Filenames updated in filenames.txt");

                                                    // 4. Read the new file, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
                                                    fs.readFile("lipsum_lowercase_sentences.txt", "utf8", (err, data) => {
                                                        if (err) {
                                                            console.log(err);
                                                        } else {
                                                            const sortedContent = data.split('\n').sort().join('\n');

                                                            // Write sorted content to a new file
                                                            fs.writeFile("lipsum_sorted.txt", sortedContent, (err) => {
                                                                if (err) {
                                                                    console.log(err);
                                                                } else {
                                                                    console.log("Sorted content written to lipsum_sorted.txt");
                                                                    console.log("********************");

                                                                    // Update the filenames in filenames.txt
                                                                    fs.writeFile("filenames.txt", "lipsum_uppercase.txt\nlipsum_lowercase_sentences.txt\nlipsum_sorted.txt\n", (err) => {
                                                                        if (err) {
                                                                            console.log(err);
                                                                        } else {
                                                                            console.log("Filenames updated in filenames.txt");

                                                                            // 5. Read the contents of filenames.txt and delete all the new files mentioned in that list simultaneously.
                                                                            fs.readFile("filenames.txt", "utf8", (err, data) => {
                                                                                if (err) {
                                                                                    console.log(err);
                                                                                } else {
                                                                                    const filesToDelete = data.split('\n').filter(file => file.trim() !== '');
                                                                                    filesToDelete.forEach(file => {
                                                                                        fs.unlink(file, (err) => {
                                                                                            if (err) {
                                                                                                console.log(err);
                                                                                            } else {
                                                                                                console.log(`${file} deleted successfully`);
                                                                                            }
                                                                                        });
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

module.exports=problem_2;
// problem_2();


