
const fs = require('fs');
const path = require('path');
function problem_1(){ 

function generateRandomJSONFiles(directory, fileCount, callback) {
    // Create directory if it doesn't exist
    fs.mkdir(directory, { recursive: true }, (err) => {
        if (err) return callback(err);

        let filesCreated = 0;

        // Generate random JSON files
        for (let i = 1; i <= fileCount; i++) {
            const fileName = `random_${i}.json`;
            generateRandomJSONFile(directory, fileName, (err) => {
                if (err) {
                    console.log(`Error creating file ${fileName}: ${err}`);
                }
                filesCreated++;

                // Check if all files are created
                if (filesCreated === fileCount) {
                    deleteFiles(directory, callback);
                }
            });
        }
    });
}

function generateRandomJSONFile(directory, fileName, callback) {
    const data = "creating and deleting are done in simultaneously";

    fs.writeFile(path.join(directory, fileName), data, (err) => {
        if (err) {
            callback(err);
        } else {
            console.log(`File ${fileName} created successfully`);
            callback(null);
        }
    });
}

function deleteFiles(directory, callback) {
    fs.readdir(directory, (err, files) => {
        if (err) return callback(err);

        let filesDeleted = 0;

        // Delete files
        files.forEach((file) => {
            fs.unlink(path.join(directory, file), (err) => {
                if (err) {
                    console.log(`Error deleting file ${file}: ${err}`);
                } else {
                    console.log(`File ${file} deleted successfully`);
                }
                filesDeleted++;
            });
        });
    });
}


// Usage example
const directory = 'random_files';
const fileCount = 2;

generateRandomJSONFiles(directory, fileCount);
}
module.exports=problem_1;




